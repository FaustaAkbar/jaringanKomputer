import socket #line 1: Mengimpor modul 'socket'.

s = socket.socket() #line 2: Membuat objek socket dengan protokol default, yaitu TCP.
host = socket.gethostname() #line 3: Mendapatkan nama host mesin lokal.
port = 9999 #line 4: Menetapkan nomor port yang akan digunakan oleh server.

s.bind((host,port)) #line 5: Mengikat socket ke alamat host dan port yang telah ditentukan.

print("Waiting for connection...")
s.listen(5) #line 6: Mulai mendengarkan koneksi dengan antrian maksimal sebanyak 5 klien.

while True: 
    conn, addr = s.accept() #line 7: Menerima koneksi dari klien dan mendapatkan objek socket dan alamat klien.
    print('Got Connection from', addr) 
    conn.send(b'Server Saying Hi') 
    conn.close() #line 8: Menutup koneksi dengan klien.
